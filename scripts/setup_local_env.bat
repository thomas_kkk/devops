@echo off
goto check_Permissions

:check_Permissions
    echo Administrative permissions required. Detecting permissions...

    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo Success: Administrative permissions confirmed.
        echo Installing Chocolately package manager
        "%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
        echo Installing apps... This will take a while.
        choco install git python notepadplusplus pycharm-community 7zip docker-desktop -y
        echo Generating ssh key
        set /p CompleteName="What is your complete name? (Ex.: Thomas Kuryura) "
        set /p UlEmail="What is your email? "
        ssh-keygen -t ed25519 -C "%UlEmail% -f %systemdrive%%homepath%\.ssh\id_ed25519
        echo Setting up SSH agent
        start-ssh-agent
        echo Configuring your git name and email
        git config --global --unset user.name
        git config --global --unset user.email
        git config --global user.name "%CompleteName%"
        git config --global user.email "%UlEmail%"
        echo All done.
        type %systemdrive%%homepath%\.ssh\id_ed25519.pub | clip
        echo The ssh key is copied to your clipboard go to https://gitlab.com/profile/keys and paste your SSH key (CTRL+V) and save into your profile
    ) else (
        echo Failure: Current permissions inadequate. You must run this script with admin rights.